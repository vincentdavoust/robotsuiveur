package testFonctionnel;

import robotsuiveur.Roues;

public class TestPlacementFin {
	private static final int DISTANCE_BALLE = 33; //30 dixi�mes de tour de roues
	private static final int DEMI_TOUR = 180;
	private static final int DISTANCE_FIN = 15;
	
	public static void main(String[] args) {
		Roues roues = new Roues();
		
		roues.reculer(DISTANCE_FIN);
		roues.rotationDroite(DEMI_TOUR/2);
		roues.avancer(DISTANCE_BALLE);
		roues.rotationGauche(DEMI_TOUR/2);
	}
}
