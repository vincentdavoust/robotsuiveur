package testFonctionnel;

import robotsuiveur.Pince;
/**
 * Test for closing the clamp
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestFermerPince {
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		Pince pince = new Pince();
		pince.fermer();
	}
}