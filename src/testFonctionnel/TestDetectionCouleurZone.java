package testFonctionnel;

import robotsuiveur.Capteurs;
import robotsuiveur.User;
/**
 * Test class for color detection area
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestDetectionCouleurZone {
	private static final int SLEEP_TIME = 5000;
	
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		User user =  new User();
		Capteurs capteur = new Capteurs();
		
		String color = capteur.getCouleur();
		user.printf("Couleur detectee : " + color);
		user.sleep(SLEEP_TIME);
	}
}