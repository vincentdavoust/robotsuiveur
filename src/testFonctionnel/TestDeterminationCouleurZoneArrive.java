package testFonctionnel;

import robotsuiveur.Capteurs;
import robotsuiveur.User;
/**
 * Test for determination color arrival area
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestDeterminationCouleurZoneArrive {
	private static final int SLEEP_TIME = 5000;
	
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		Capteurs capt = new Capteurs();
		User user = new User();
		
		String couleur = capt.getCouleur();
		
		if(couleur == "Blue") {
			user.printf("Zone de depart : " + couleur + "\nZone d'arrivee : Green");
		}
		else if(couleur=="Red") {
			user.printf("Zone de depart : " + couleur + "\nZone d'arrivee : Yellow");
		}
		
		user.sleep(SLEEP_TIME);
	}
}