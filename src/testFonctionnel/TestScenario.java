package testFonctionnel;

import java.util.ArrayList;

import robotsuiveur.Capteurs;
import robotsuiveur.Movement;
import robotsuiveur.Parseur;
import robotsuiveur.Pince;
import robotsuiveur.Roues;

/**
 * Scenario class test
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
 
 public class TestScenario {
 
 private static final int DISTANCE_BALLE = 25; //25 dixièmes de tour de roues
	private static final int DEMI_TOUR = 180;
	private static final int DISTANCE_FIN = 15;
	private static final int DISTANCE_DETECTION_BALLE = 60;
	private static final String CHEMIN = "PATH:F25;R90;F25;L90;F25;L90;F25;L90";

	private Roues roues;
	private Capteurs capt;
	private Pince pince;
	private boolean isGreen;
	private Parseur parse;
		
	public TestScenario(){
		capt = new Capteurs();
		roues = new Roues(capt);
		pince = new Pince();
		parse = new Parseur();
	}
		
	public static void main(String[] args) throws InterruptedException {
		TestScenario ts = new TestScenario();
		ts.trouverBalle();
		ts.chercherBalle();
		ts.retourHome();
		ts.mission(ts.parse.parsePath(CHEMIN));
		ts.deposeBalle();
		ts.exit();
	}
	
	/**
	 * Test of constructor
	 */
	private void trouverBalle() {
		roues.rotationDroite(DEMI_TOUR/2);
		if (detecterBalle()) {
			isGreen = false;
			return;
		}
		roues.rotationGauche(DEMI_TOUR);
		isGreen = true;
	}
	
	/**
	 * 
	 * Look for a ball.
	 * 
	 * @return true if the ball is close enough.
	 */
	private boolean detecterBalle() {
	  if (capt.getDistanceObstacle() < DISTANCE_DETECTION_BALLE) {
			return true;
	  }
		else {
			return false;
		}
	}
	
	private void chercherBalle() {
		pince.ouvrir();
		roues.avancer(DISTANCE_BALLE);
		pince.fermer();
	}
	
	private void retourHome() {
		roues.rotationDroite(DEMI_TOUR);
		roues.avancer(DISTANCE_BALLE);
		
		if (isGreen) {
			roues.rotationDroite(DEMI_TOUR/2);
		}
		else {
			roues.rotationGauche(DEMI_TOUR/2);
		}
	}
	
	private void mission(ArrayList<Movement> instructions) {
		for (Movement move : instructions) {
			switch (move.getAction()) {
			case 'F' :
				roues.avancer(move.getDistance());
				break;
			case 'B' :
				roues.reculer(move.getDistance());
				break;
			case 'R' :
				roues.rotationDroite(move.getDistance());
				break;
			case 'L' :
				roues.rotationGauche(move.getDistance());
				break;
			}
		}
	}
	
	private void deposeBalle() {
		pince.ouvrir();
		pince.fermer();
	}
	
	/**
	 * Launches the end of run.
	 */
	private void exit() {
		if (isGreen) {
			roues.rotationDroite(DEMI_TOUR);
			roues.avancer(DISTANCE_FIN);
			roues.rotationDroite(DEMI_TOUR/2);
			roues.avancer(DISTANCE_BALLE);
			roues.rotationDroite(DEMI_TOUR);
		} else {
			roues.rotationGauche(DEMI_TOUR);
			roues.avancer(DISTANCE_FIN);
			roues.rotationGauche(DEMI_TOUR/2);
			roues.avancer(DISTANCE_BALLE);
			roues.rotationGauche(DEMI_TOUR);
		}
	}
 }