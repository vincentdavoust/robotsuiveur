package testFonctionnel;

import robotsuiveur.Capteurs;
import robotsuiveur.User;
/**
 * Test class to test the object detection by the robot
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestDetecterObject {
	private static final int SLEEP_TIME = 200;
	
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		Capteurs capt = new Capteurs();
		User user = new User();
		int distance = 0;
		
		while(true) {
			distance = capt.getDistanceObstacle();
			user.printf(Integer.toString(distance));
			user.sleep(SLEEP_TIME);
		}
	}
}