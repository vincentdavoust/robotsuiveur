package testFonctionnel;

import robotsuiveur.Roues;
/**
 * Test class to test the ability of the to keep a ball
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestConserverBalle {
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		Roues r = new Roues();
		
		r.avancer(50);
		r.rotationDroite(90);
		r.reculer(50);
		r.rotationGauche(90);
		r.avancer(50);
	}
}