package testFonctionnel;

import robotsuiveur.Roues;
/**
 * Moving forward test class
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestAvancer {
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		Roues roue = new Roues();
		roue.avancer(50);
	}
}