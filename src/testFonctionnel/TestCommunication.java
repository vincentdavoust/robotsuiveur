package testFonctionnel;

import robotsuiveur.Communication;
/**
 * Communication test class
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestCommunication {
	/**
	 * main function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Communication comm = new Communication();
		comm.initConnexion(60000);
		comm.sendColor("Red");
		comm.sendReady();
		comm.getInstructions();
		comm.sendStart();
		comm.waitOK();
	}
}