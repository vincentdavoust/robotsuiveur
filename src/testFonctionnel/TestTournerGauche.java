package testFonctionnel;

import robotsuiveur.Capteurs;
import robotsuiveur.Roues;
import robotsuiveur.User;
/**
 * Turn left test
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestTournerGauche {
	private static final int SLEEP_TIME = 3000;
	
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		Capteurs capt = new Capteurs();
		Roues roue = new Roues(capt);
		User user = new User();
		
		int angleInitial = capt.getAngle();
		user.printf("Angle initial: " + Integer.toString(angleInitial));
		user.sleep(SLEEP_TIME);
		
		roue.rotationGauche(90);
		
		user.printf("Angle final: " + Integer.toString(angleInitial-capt.getAngle()));
		user.sleep(SLEEP_TIME);
	}
}