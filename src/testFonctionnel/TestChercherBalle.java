package testFonctionnel;

import robotsuiveur.Capteurs;
import robotsuiveur.Pince;
import robotsuiveur.Roues;

public class TestChercherBalle {
	private static final int DISTANCE_BALLE = 25; //25 dixi�mes de tour de roues
	private static final int DEMI_TOUR = 180;
	private static final int DISTANCE_DETECTION_BALLE = 60;
	
	public static void main(String[] args) {
		Capteurs capt = new Capteurs();
		Roues roues = new Roues(capt);
		Pince pince = new Pince();
		
		roues.rotationDroite(DEMI_TOUR/2);
		
		if (capt.getDistanceObstacle() >= DISTANCE_DETECTION_BALLE) {
			roues.rotationGauche(DEMI_TOUR);
		}
		
		pince.ouvrir();
		roues.avancer(DISTANCE_BALLE);
		pince.fermer();
		roues.reculer(DISTANCE_BALLE);
	}
}
