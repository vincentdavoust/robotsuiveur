package testFonctionnel;

import robotsuiveur.Roues;
/**
 * Backwards test
 * 
 * @author Groupe Suiveur
 * @version 1.4
 *
 */
public class TestReculer {
	/**
	 * main function
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) {
		Roues roue = new Roues();
		roue.reculer(50);
	}
}