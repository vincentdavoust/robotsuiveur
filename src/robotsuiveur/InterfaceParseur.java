package robotsuiveur;
import java.util.ArrayList;

/**
 * Interface of the Parser.
 * 
 * @author Groupe suiveur
 * @version 1.4
 */
public interface InterfaceParseur {
  /**
   * Convert a string to a hashmap of instructions with key "action" and value
   * "distance"
   * The expected form for the message is :
   * "PATH:"+(('F'|'B'|'L'|'R') + /distance)*+";"
   * The leters 'F', 'B', 'L', 'R' correspond to the instructions to move forwards, backwards, left, or ight respectivly.
   * /distance will be an integer corresponding to the distance to apply to the specified move
   * the sign '+' represents concatenationTe expected form for the message is :
   * "PATH:"+(('F'|'B'|'L'|'R') + /distance)*+";"
   * The leters 'F', 'B', 'L', 'R' correspond to the instructions to move forwards, backwards, left, or ight respectivly.
   * /distance will be an integer corresponding to the distance to apply to the specified move
   * the sign '+' represents concatenation
   *
   * @param        input Path string to parse
   */
  public ArrayList<Movement> parsePath(String input);
}
