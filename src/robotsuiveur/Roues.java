package robotsuiveur;

import lejos.hardware.motor.*;
import lejos.hardware.port.MotorPort;

/**
 * Wheel class which implements the robot's wheels.
 * It handles and defines the movements such as :
 * Move forward
 * Move backward
 * Rotate left or right
 * Recalibrate the angle of the wheels
 * Set motors speed
 * 
 * @author GroupeSuiveur
 * @version 1.4
 *br/>
 *<img src="./doc-files/Roues.png"/>
 *
 * The previous picture explain the relations between this class and 
 * the entire project. This class is working with the control robot 
 * class.
 */

public class Roues implements InterfaceRoues  {
	/**
	 * Varibles used for the Wheels.
	 */
	private static final int SLEEP_TIME = 10;
	private static final int MOTOR_SPEED_DIVIDE = 4;
	private static final int MOTOR_SPEED_SLOW_DIVIDE = 8;
	private static final int MARGIN_ROTATION = 5;
	private static final int ROTATION_ANGLE_CALIBRATION = 1;
	private static final int EMERGENCY_DISTANCE = 9;
	
	private InterfaceCapteurs sensors;
	private BaseRegulatedMotor rightMotor;
	private BaseRegulatedMotor leftMotor;
	private User user;
	
	/**
	 * Basic constructor of wheel class.
	 */
    public Roues() {
        this(new Capteurs());
    }
    /**
     * Constructor of Wheel class. Instanciates the class with the given sensor.
     * 
     * @param sensors
     */
	public Roues(InterfaceCapteurs sensors) {
		this.sensors = sensors;
		
		rightMotor = new EV3LargeRegulatedMotor(MotorPort.A);
		rightMotor.setSpeed(rightMotor.getMaxSpeed()/MOTOR_SPEED_DIVIDE);
		
		leftMotor = new EV3LargeRegulatedMotor(MotorPort.B);
		leftMotor.setSpeed(leftMotor.getMaxSpeed()/MOTOR_SPEED_DIVIDE);
		
		user = new User();
	}
	
	/**
	 * Moves forward at the given distance.
	 * 
	 * @param distance distance to move forward
	 */
	public void avancer(int distance) {
		rightMotor.rotate(36 * distance, true);
		leftMotor.rotate(36 * distance, true);
		while (rightMotor.isMoving()) {
			user.sleep(SLEEP_TIME);
		}
	}
	
	/**
	 * Moves forward at the given distance, while being safe or not.
	 * 
	 * @param distance distance to move forward
	 * @param safe if true moves only if there is no obstacle before EMERGENCY_DISTANCE
	 */
	public void avancer(int distance, boolean safe) {
		rightMotor.rotate(36 * distance, true);
		leftMotor.rotate(36 * distance, true);
		while (rightMotor.isMoving()) {
			if (safe && this.sensors.getDistanceObstacle() < EMERGENCY_DISTANCE) {
				rightMotor.stop(false);
				leftMotor.stop(false);
				return ;
			}
		}
	}

	/**
	 * Moves back at the given distance.
	 * 
	 * @param distance distance to move backward.
	 */
	public void reculer(int distance) {
		avancer(-distance);
	}

	/**
	 * Rotate to the right at the given angle.
	 * 
	 * @param degres angle to move at
	 */
	public void rotationDroite(int degres) {
		setSpeed(MOTOR_SPEED_SLOW_DIVIDE);
		int pos = sensors.getAngle();
		
        rightMotor.backward();
		leftMotor.forward();
		
		while(sensors.getAngle() > (pos - degres + MARGIN_ROTATION)) {
			user.printf("Giro while "+sensors.getAngle());
        }
		
		rightMotor.stop(false);
		leftMotor.stop(false);
		
		recalibrate(pos - degres);
		
        setSpeed(MOTOR_SPEED_DIVIDE);
	}

	/**
	 * Rotate to the left at the given angle.
	 * @param degres angle to move at.
	 */
	public void rotationGauche(int degres) {
        setSpeed(MOTOR_SPEED_SLOW_DIVIDE);
		int pos = sensors.getAngle();
		
		rightMotor.forward();
		leftMotor.backward();
		 
        while(sensors.getAngle() < (pos + degres - MARGIN_ROTATION)) {
        	user.printf("Giro while "+sensors.getAngle());
        }
                
		rightMotor.stop(false);
		leftMotor.stop(false);
		
		recalibrate(pos + degres);
		
        setSpeed(MOTOR_SPEED_DIVIDE);
	}
	/**
	 * Recalibrates the current angle to the correct angle that was initially to move at.
	 * 
	 * @param pos angle to move at.
	 */
	public void recalibrate(int pos) {
		//pos = pos%360;
		while(sensors.getAngle() != pos) {
			//if(sensors.getAngle()%360 > pos  || (sensors.getAngle()%360 > 180 && sensors.getAngle()%360 > pos)) {
			if (sensors.getAngle() > pos) {
				rightMotor.rotate(-ROTATION_ANGLE_CALIBRATION);
				leftMotor.rotate(ROTATION_ANGLE_CALIBRATION);
			}
			else {
				rightMotor.rotate(ROTATION_ANGLE_CALIBRATION);
				leftMotor.rotate(-ROTATION_ANGLE_CALIBRATION);
			}
		}
		
		user.printf("Giro while "+sensors.getAngle());
	}
    /**
     * Divide the current speed of both motors by the given number i.
     * 
     * @param i number by which to divide the speed
     */
    private void setSpeed(int i){
		leftMotor.setSpeed(leftMotor.getMaxSpeed()/i);
        rightMotor.setSpeed(rightMotor.getMaxSpeed()/i);   
    }
}