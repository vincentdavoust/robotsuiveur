package robotsuiveur;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * Interface of Communication.
 * The physical transport method will be a Bluetooth connection. 
 * The communications will be encrypted.
 * 
 * @author Groupe suiveur
 * @version 1.4
 */
public interface InterfaceCom {
  /**
   * Sends the color to the other robot.
   * It is called when the robot gets the ball and knows  which is the starting color.
   * It will transmit a string "GREEN" or "ORANGE" corresponding to the color of the starting square.
   * 
   * @param color String color to send.
   */
  public void sendColor(String color);


  /**
   * Tells the other robot that the robot is ready.
   * 
   */
  public void sendReady();


  /**
   * Returns a list of instructions received from the other robot.
   * getInstructions() is called when the robot has come back to it's original location
   * It will listen to the interface until a sequence of moves is transmitted.
   * The expected form for the message is :
   * "PATH:"+(('F'|'B'|'L'|'R') + /distance)*+";"
   * The leters 'F', 'B', 'L', 'R' correspond to the instructions to move forwards, backwards, left, or ight respectivly.
   * /distance will be an integer corresponding to the distance to apply to the specified move
   * the sign '+' represents concatenation
   */
  public ArrayList<Movement> getInstructions();


  /**
   * Sends start.
   */
  public void sendStart();


  /**
   * Wait for the other robot to be ready.
   * It is called after sendColor(). 
   * A string containing "OK" must be received before the robot returns to its starting coordinates.	
   * @throws IOException 
   */
  public void waitOK() throws IOException;
}
