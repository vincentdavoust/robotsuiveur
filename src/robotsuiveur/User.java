package robotsuiveur;

import java.io.File;
import lejos.hardware.Sound;
import lejos.hardware.lcd.LCD;
import lejos.hardware.Button;

/**
 * Roues class implements the interface InterfaceRoues and define the differents modes of robot movement
 * 
 * 
 * @author GroupeSuiveur
 * @version 1.4
<<<<<<< HEAD
=======
 *<br/>
 *<img src="./doc-files/User.png"/>
 *
 * The previous picture explain the relations between this class and 
 * the entire project. This class is linked to control robot. It 
 * capture the button and display string on the screen.
>>>>>>> 885311054581743335aff77ac137ceb0407f056e
 *
 */

public class User implements InterfaceUser {
	private final int SCREEN_LEN = 16;
	
	public void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Byte getButton() {
		return (byte)Button.getButtons();
	}

	@Override
	public void printf(String s) {
		int j = 1;
		
		LCD.clear();
		
		while(s.length() > SCREEN_LEN) {
			LCD.drawString(s.substring(0, SCREEN_LEN), 0, j);
			j++;
			s = s.substring(SCREEN_LEN, s.length());
		}
		LCD.drawString(s, 0, j);
		LCD.refresh();
	}

	/**
	 *  Method who play music on user request
	 */
	
	public void music() {
		Sound.systemSound(true, 2);
	}
}
