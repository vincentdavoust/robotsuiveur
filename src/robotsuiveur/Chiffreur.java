package robotsuiveur;

import sun.misc.BASE64Encoder;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * Encoder class implementing Encoding and decoding functionalities for 
 * string messages.
 * 
 * @author Groupe suiveur
 * @version 1.4
 *<br/>
 *<img src="./doc-files/Chiffreur.png"/>
 *
 * The previous picture explain the relations between this class and 
 * the entire project. This class is waiting data before processing.
 * It works with the communication class.
 */
public class Chiffreur implements InterfaceChiff {
	/**
	 * Encoding key to encrypt the data
	 * Data are encrypt in AES
	 */
	static final String cle = "1233211233211233";

	@Override
	public String chiffrer(String input) throws Exception {
		
	    Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec key = new SecretKeySpec(cle.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedBytes = cipher.doFinal(input.getBytes());
        return new BASE64Encoder().encode(encryptedBytes);
	}

	@Override
	public String dechiffrer(String input) throws Exception {
		
		Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec key = new SecretKeySpec(cle.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] cipherTextBytes = new BASE64Decoder().decodeBuffer(input);
        byte[] decValue = cipher.doFinal(cipherTextBytes);
        return new String(decValue);
	}
}
