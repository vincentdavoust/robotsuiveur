package robotsuiveur;
/**
 * Interface User interface.
 */
public interface InterfaceUser {
  /**
   * returns the pressed buttons, with each bit corresponding to a button.
   * 
   * @return       Byte
   */
  public Byte getButton();


  /**
   * Prints a string.
   * 
   * @param        s String to print
   */
  public void printf(String s);
  
  /**
   * Play the music of the robot.
   */
  public void music();
}