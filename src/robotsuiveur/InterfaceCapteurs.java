package robotsuiveur;

/**
 * Interface InterfaceCapteurs
 * 
 * @author Groupe suiveur
 * @version 1.4
 */
public interface InterfaceCapteurs {
  /**
   * Returns the color from the colorSensor.
   * 
   * @return String color received from the sensor
   */
  public String getCouleur();


  /**
   * Returns the distance between robot and the obstacle.
   * 
   * @return int distance between robot and the obstacle
   */
  public int getDistanceObstacle();


  /**
   * Returns the angle from the starting position.
   * 
   * @return Integer angle from the starting position
   */
  public int getAngle();
}