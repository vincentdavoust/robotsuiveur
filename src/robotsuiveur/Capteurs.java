package robotsuiveur;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;

/**
 * Sensor class implementing sensors functionalities wich are :
 * color detection.
 * obstacle detection, more precisely distance to obstacle.
 * angle to original location detection.
 * 
 * @author Groupe suiveur
 * @version 1.4
 * <br/>
 *<img src="./doc-files/Capteur.png"/>
 *
 * The previous picture explain the relations between this class and 
 * the entire project. Capteurs is working with control robot. 
 * It capture distances, colors and angles. 
 * It send that data to the control robot.
 */
public class Capteurs implements InterfaceCapteurs {
	/**
	 * Every colors the robot can handle.
	 */
	private static final String COULEUR_ROUGE = "Red";
	private static final String COULEUR_VERT = "Green";
	private static final String COULEUR_BLEU = "Blue";
	private static final String COULEUR_YELLOW = "Yellow";
	private static final String COULEUR_NONE = "None";
	
	/**
	 * Waiting time after some text display
	 */
	private static final int SLEEP_TIME = 500;
	
	/**
	 * The 3 sensor types.
	 */
	private EV3GyroSensor gyroSensor;
	private EV3UltrasonicSensor ultrasonicSensor;
	private EV3ColorSensor colorSensor;
	
	/**
	 * Class user used to display texts on the LCD screen
	 */
	private User user;
	
	/**
	 * Initializes the Sensors
	 */
	public Capteurs() {
		user = new User();
		Brick brick = BrickFinder.getDefault();
		boolean success = false;
		
		while (success == false) {
			success = true;
			try {
				this.gyroSensor = new EV3GyroSensor(brick.getPort("S1"));
				user.printf("Gyroscope initialise");
			} catch(Exception e) {
				user.printf("Impossible d'initialiser le gyroscope");
				success = false;
				continue ;
			}
			user.sleep(SLEEP_TIME);
			
			try {
				this.ultrasonicSensor = new EV3UltrasonicSensor(brick.getPort("S2"));
				user.printf("Capteur ultrason initialise");
			} catch(Exception e) {
				user.printf("Impossible d'initialiser le capteur ultrason");
				gyroSensor.close();
				success = false;
				continue ;
			}
			user.sleep(SLEEP_TIME);
			
			try {
				this.colorSensor = new EV3ColorSensor(brick.getPort("S4"));
				user.printf("Capteur de couleurs initialise");
			} catch(Exception e) {
				user.printf("Impossible d'initialiser le capteur de couleurs");
				gyroSensor.close();
				ultrasonicSensor.close();
				success = false;
				continue ;
			}
			user.sleep(SLEEP_TIME);
		}
	}
	
	@Override
	public String getCouleur() {
		int res = this.colorSensor.getColorID();
		
		if(res == Color.BLUE) {
			return COULEUR_BLEU;
		}
		else if(res == Color.GREEN) {
			return COULEUR_VERT;
		}
		else if(res == Color.YELLOW) {
			return COULEUR_YELLOW;
		}
		else if(res == Color.RED) {
			return COULEUR_ROUGE;
		}
		else {
			return COULEUR_NONE;
		}
	}

	@Override
	public int getDistanceObstacle() {
		SampleProvider sample = this.ultrasonicSensor.getDistanceMode();
		float[] distance = new float[1];
		
		//The sample contains one elements representing the distance (in meters) to an object in front of the sensor. unit).
		sample.fetchSample(distance, 0);
		return (int) (distance[0]*100);
	}
	
	@Override
	public int getAngle() {
		SampleProvider sample = this.gyroSensor.getAngleMode();
		float[] angle = new float[1];
		
		//The sample contains one elements representing the orientation (in Degrees) of the sensor in respect to its start position. 
		sample.fetchSample(angle, 0);
		return  (int) angle[0];
	}
}