package robotsuiveur;

/**
 * Movement class implementing functionalities such as :
 * 	Creating a Movement object wich specify :
 * 	The type of Movement
 * 	The distance of the Movement.
 * 	
 * 
 * @author GroupeSuiveur
 * @version 1.4
 *
 */

public class Movement {

    //    public enum Actions { 'F', 'B', 'R', 'L'}

	private char action;
	private int distance;
	
	/**
	 * Constructor of movement class
	 * 
	 * @param action
	 * @param distance
	 */

	public Movement(char action, int distance) {
		this.action = action;
		this.distance = distance;
	}

	/**
	 * Returns the action of the Movement.
	 * 
	 * @return action of Movement
	 */
	public char getAction() {
		return action;
	}
	
	/**
	 * Returns the distance of the Movement.
	 * 
	 * @return distance of Movement
 	 */
	public int getDistance() {
		return distance;
	}
}
