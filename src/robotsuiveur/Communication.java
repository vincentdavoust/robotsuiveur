package robotsuiveur;
import java.util.ArrayList;
import lejos.hardware.lcd.LCD;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import lejos.hardware.Brick;

public class Communication implements InterfaceCom{
    private Brick[] bricks;
    private static final int PORT = 5678;
    private Socket s;
    private ServerSocket cmdSock;
    
    public Communication() {
        initConnexion(60000);
    }
    
    
    public boolean initConnexion(int timeout) {
        
        LCD.drawString("waiting for BT", 0,1 );
        
        try {
            cmdSock = new ServerSocket(PORT);
            s = cmdSock.accept();
        } catch (IOException ex) {
            LCD.drawString("Failed", 0, 0);
            return false;
        }
        
        LCD.drawString("connected", 0, 0);
        return true;
    }
    
    @Override
    public void sendColor(String color) {
        try {
            s.getOutputStream().write(color.getBytes());
        } catch (IOException e ) {
            LCD.drawString(" write error "+e,0,2);
            
        }
        
    }
    
    @Override
    public void sendReady() {
        try {
            s.getOutputStream().write("READY".getBytes());
        } catch (IOException e ) {
            LCD.drawString(" write error "+e,0,2);
            
        }
    }
    
    
    
    @Override
    public ArrayList<Movement> getInstructions(){
        try {
            ArrayList<Movement> List = null;
            byte[] tmp = new byte[1000];
            int i = s.getInputStream().read(tmp);
            String recu = new String(tmp,0,i, "UTF-8");
            String clair;
            Chiffreur chiff = new Chiffreur();
            clair = chiff.dechiffrer(recu);
            List = (new Parseur()).parsePath(clair);
            return List;
        } catch (Exception ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void sendStart() {
        try {
            s.getOutputStream().write("START".getBytes());
        } catch (IOException e ) {
            LCD.drawString(" write error "+e,0,2);
            
        }
    }
    

    @Override
    public void waitOK(){
        String recu;
        try {
            byte[] tmp = new byte[12];
            s.getInputStream().read(tmp);
            recu = new String(tmp, "UTF-8");
            LCD.drawString(recu, 0, 3);
            while(!recu.contains("ok") ){
                s.getInputStream().read(tmp);
                recu = new String(tmp, "UTF-8");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    

}
