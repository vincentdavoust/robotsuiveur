package robotsuiveur;

/**
 * Interface of Encoder
 * 
 * @author Groupe suiveur
 * @version 1.4
 */
public interface InterfaceChiff {
  /**
   * Encrypt a string
   * @return       String
   * @param        Input Clear text string
   * @throws Exception 
   */
  public String chiffrer(String Input) throws Exception;


  /**
   * Dechifer a string
   * @return       String
   * @param        Input Encrypted string
   * @throws Exception 
   */
  public String dechiffrer(String Input) throws Exception;
  
}