package robotsuiveur;
/**
 * Interface of wheel
 * 
 * @author Groupe suiveur
 * @version 1.4
 */
public interface InterfaceRoues {
  /**
   * Move forward.
   * 
   * @param        distance to move forward.
   */
  public void avancer(int distance);


  /**
   * Move back.
   * @param        distance to move back.
   */
  public void reculer(int distance);


  /**
   * Rotate the robot of given angle to the right.
   * @param        degres angle to rotate
   */
  public void rotationDroite(int degres);


  /**
   * Rotate the robot of given angle to the left.
   * @param        degres angle to rotate
   */
  public void rotationGauche(int degres);
}