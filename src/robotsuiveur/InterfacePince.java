package robotsuiveur;
/**
 * Interface of clamp
 * 
 * @author Groupe suiveur
 * @version 1.4
 * 
 */
public interface InterfacePince {
  /**
   * Open the clamp.
   */
  public void ouvrir();


  /**
   * Close the clamp.
   */
  public void fermer();
}