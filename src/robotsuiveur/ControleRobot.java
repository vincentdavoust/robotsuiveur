package robotsuiveur;
import java.util.ArrayList;
/**
 * 
 * Robot Control Main class. 
 * It runs the main program which handle whole robot.
 * It starts many operations in order to :
 * 	Find the ball.
 * 	Retreive the ball.
 * 	Wait for communications.
 * 	Move back to initial location.
 * 	Wait for instructions.
 * 	Execute instructions.
 * 
 * @author Groupe suiveur
 * @version 1.4
 * <br/>
 * <img src="./doc-files/ControlRobot.png"/>
 *
 * The previous picture explain the relations between this class and 
 * the entire project. Robot Control is the main class of the project
 * and is linked to User, Clamp, Wheels, Sensors and Communication
 *
 */
public class ControleRobot {
	/**
	 * Distances used for controlling the robot.
	 */
	private static final int DISTANCE_BALLE = 25; //25 dixi�mes de tour de roues
	private static final int DISTANCE_FIN_X = 31; //33 dixi�mes de tour de roues
	private static final int DEMI_TOUR = 180;
	private static final int DISTANCE_FIN_Y = 15;
	private static final int DISTANCE_DETECTION_BALLE = 60;

	private Communication comm;
	private Roues roues;
	private Capteurs capt;
	private Pince pince;
	private User user;
	private boolean isGreen;
	private int initAngle;
	/**
	 * Control class's contructor.
	 */
	public ControleRobot() {
		comm = new Communication();
		capt = new Capteurs();
		roues = new Roues(capt);
		pince = new Pince();
		user = new User();
		initAngle = capt.getAngle();
		
		run();
	}
	/**
	 * 
	 * Main function of the class.
	 * 
	 * @param args arguments
	 */
	public static void main(String[] args) {
		new ControleRobot();
	}
	/**
	 * Run function of the class. Launches every functions needed for the projet.
	 */
	public void run() {
		waitStart();
		trouverBalle();
		chercherBalle();
		attenteOK();
		retourHome();
		roues.recalibrate(initAngle);
		mission(attenteInstructions());
		deposeBalle();
		exit();
		fin();
	}

	/**
	 * Wait for start event
	 */
	private void waitStart() {

	}

	/**
	 * scan both sides to find where the ball is. leave the robot facing the ball
	 */
	private void trouverBalle() {
		roues.rotationDroite(DEMI_TOUR/2);
		if ((isGreen = detecterBalle()) == false) {
			roues.rotationGauche(DEMI_TOUR);
		}
	}
	
	/**
	 * 
	 * Look for a ball.
	 * 
	 * @return true if the ball is close enough.
	 */
	private boolean detecterBalle() {
	  if (capt.getDistanceObstacle() < DISTANCE_DETECTION_BALLE) {
			return true;
	  } else {
			return false;
	  }
	}
	/**
	 * Go to the ball, collect it, scan the color of the base, send the color to the guide robot
	 */
	
	private void chercherBalle() {
		// preparer la pince
	  pince.ouvrir();
		// aller a la balle
		roues.avancer(DISTANCE_BALLE);
		// lire couleur
		String couleur = capt.getCouleur();
		// prendre la balle
		pince.fermer();
		// envoyer couleur
		comm.sendColor(couleur);
	}
	/**
	 * Wait for the communications to be ready.
	 */
	private void attenteOK() {
	  comm.waitOK();
	}
	/**
	 * Move the robot back home.
	 */
	private void retourHome() {
		if (isGreen) {
			roues.rotationGauche(DEMI_TOUR);
		} else {
			roues.rotationDroite(DEMI_TOUR);
		}
		
		roues.avancer(DISTANCE_BALLE);
		
		if (isGreen) {
			roues.rotationDroite(DEMI_TOUR/2);
		}
		else {
			roues.rotationGauche(DEMI_TOUR/2);
		}
	}
	/**
	 * 
	 * Wait for instrucitons
	 * 
	 * @return ArrayList<Movement> List of Movements received.
	 */
	private ArrayList<Movement> attenteInstructions() {
    return comm.getInstructions();
	}
	/**
	 * 
	 * Execute the list of instructions.
	 * 
	 * @param instructions list of Movement instructions.
	 */
	private void mission(ArrayList<Movement> instructions) {
		for (Movement move : instructions) {
			switch (move.getAction()) {
			case 'F' :
				roues.avancer(move.getDistance());
				break;
			case 'B' :
				roues.reculer(move.getDistance());
				break;
			case 'R' :
				roues.rotationDroite(move.getDistance());
				break;
			case 'L' :
				roues.rotationGauche(move.getDistance());
				break;
			}
		}
	}
	/**
	 * Drop the ball.
	 */
	private void deposeBalle() {
		pince.ouvrir();
		pince.fermer();
	}
	/**
	 * Launches the end of run.
	 */
	private void exit() {
		roues.reculer(DISTANCE_FIN_Y);
		if (isGreen) {
			roues.rotationDroite(DEMI_TOUR/2);
			roues.avancer(DISTANCE_FIN_X);
			roues.rotationGauche(DEMI_TOUR/2);
		} else {
			roues.rotationGauche(DEMI_TOUR/2);
			roues.avancer(DISTANCE_FIN_X);
			roues.rotationDroite(DEMI_TOUR/2);
		}
	}
	/**
	 * End.
	 */
	private void fin() {
		user.music();
	}
}