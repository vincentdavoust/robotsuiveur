package robotsuiveur;

import lejos.hardware.motor.BaseRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;

/**
 * Clamp class implements InterfacePince interface which implements functionalities such as 
 * opening and closing the clamp.
 * 
 * @author GroupeSuiveur
 * @version 1.4
 * <br/>
 * <img src="./doc-files/Pince.png"/>
 * The previous picture explain the relations between this class and 
 * the entire project. This class is working with the control robot 
 * class. The clamp can be opened or closed.
 */

public class Pince implements InterfacePince {
	private static final int ROTATION = 3*360;
	private static final int SLEEP_TIME = 10;
	
	private BaseRegulatedMotor motor;
	private User user;
	/**
	 * Constructor of Clamp class.
	 */
	public Pince() {
		motor = new EV3MediumRegulatedMotor(MotorPort.C);
		user = new User();
	}

	/**
	 * Closes the clamp.
	 */
	public void fermer(){
		motor.rotate(-ROTATION);
		while (motor.isMoving()) {
			user.sleep(SLEEP_TIME);
		}
	}
	
	/**
	 * Opens the clamp.
	 */
	public void ouvrir(){
		motor.rotate(ROTATION);
		while (motor.isMoving()) {
			user.sleep(SLEEP_TIME);
		}
	}
}