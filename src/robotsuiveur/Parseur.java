package robotsuiveur;
import java.util.ArrayList;
/**
 * 
 * Parser class of following robot which translate a String into a Movement object.
 * 
 * @author Groupe Suiveur
 * @version 1.4
 * <br/>
 *<img src="./doc-files/Parseur.png"/>
 *
 * The previous picture explain the relations between this class and 
 * the entire project. It works with communication, it receive data 
 * and work on it.
 */
public class Parseur implements InterfaceParseur {
	
	@Override
	public ArrayList<Movement> parsePath(String input) {
		ArrayList<Movement> myList = new ArrayList<>();
		
		if(input.contains("PATH:")) {
			input = input.substring(5);

			String[] parts = input.split(";");
			for (String string : parts) {
				myList.add(new Movement(string.toCharArray()[0], Integer.parseInt(string.substring(1))));
			}
		}
		
		return myList;
	}
}