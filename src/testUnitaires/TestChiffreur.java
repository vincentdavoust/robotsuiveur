package testUnitaires;

import static org.junit.Assert.*;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;

import robotsuiveur.*;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 
 * Test class to encrypt and decrypt data in AES
 *
 * 
 * @author Groupe suiveur
 * @version 1.4
 *
 */

public class TestChiffreur {

	
	
	final String KEY = "1233211233211233";
	final String CHAINE1 = "bonjour";
	final String CHAINE2 = "ceci est un texte tres tres long";
	/**
	 * Method to encrypt data
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testChiffrer() throws Exception{
		Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec key = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        
        byte[] encryptedBytes1 = cipher.doFinal(CHAINE1.getBytes());
        String result1 = new BASE64Encoder().encode(encryptedBytes1);
        
        byte[] encryptedBytes2 = cipher.doFinal(CHAINE2.getBytes());
        String result2 = new BASE64Encoder().encode(encryptedBytes2);
        
        Chiffreur chif = new Chiffreur();
        
        assertEquals(result1, chif.chiffrer(CHAINE1));
        assertEquals(result2, chif.chiffrer(CHAINE2));
        assertNotEquals(result2, chif.chiffrer(CHAINE1));
        assertNotEquals(result1, chif.chiffrer(CHAINE2));
	}
	/**
	 * Method to derypt data
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testDechiffrer() throws Exception{
		Cipher cipher1 = Cipher.getInstance("AES");
        SecretKeySpec key1 = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");
        cipher1.init(Cipher.ENCRYPT_MODE, key1);
        
        byte[] encryptedBytes1 = cipher1.doFinal(CHAINE1.getBytes());
        String chif1 = new BASE64Encoder().encode(encryptedBytes1);
        
        byte[] encryptedBytes2 = cipher1.doFinal(CHAINE2.getBytes());
        String chif2 = new BASE64Encoder().encode(encryptedBytes2);
        
        Cipher cipher2 = Cipher.getInstance("AES");
        SecretKeySpec key2 = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");
        cipher2.init(Cipher.DECRYPT_MODE, key2);
        
        byte[] cipherTextBytes2 = new BASE64Decoder().decodeBuffer(chif1);
        byte[] decValue = cipher2.doFinal(cipherTextBytes2);
        String result1 = new String(decValue);
        
        cipherTextBytes2 = new BASE64Decoder().decodeBuffer(chif2);
        decValue = cipher2.doFinal(cipherTextBytes2);
        String result2 = new String(decValue);
        
        Chiffreur chif = new Chiffreur();
        
        assertEquals(result1, chif.dechiffrer(chif1));
        assertEquals(result2, chif.dechiffrer(chif2));
        assertNotEquals(result2, chif.dechiffrer(chif1));
        assertNotEquals(result1, chif.dechiffrer(chif2));
	}
}
