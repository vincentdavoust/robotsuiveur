package testUnitaires;
import org.junit.Test;

import robotsuiveur.*;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

/**
 * Test class of Parseur class 
 * 
 * @author GroupeSuiveur
 * @version 1.4
 *
 */

public class TestParseur {
	private String str = "L45;F76";

	/**
	 * Test of constructor
	 */
	@Test
	public void testParsePath() throws Exception {
		ArrayList<Movement> myList = new ArrayList<>();
		String[] parts = str.split(";");
		for (String string : parts) {
			myList.add(new Movement(string.toCharArray()[0], Integer.parseInt(string.substring(1))));
		}

		Parseur p = new Parseur();
		ArrayList<Movement> res = p.parsePath("PATH:L45;F76");
		for (int i = 0; i < myList.size(); i++) {
			assertEquals(myList.get(i).getAction(), res.get(i).getAction());
			assertEquals(myList.get(i).getDistance(), res.get(i).getDistance());
		}
	}
}