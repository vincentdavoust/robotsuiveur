package testUnitaires;
import static org.junit.Assert.*;
import org.junit.Test;

import robotsuiveur.*;

/**
 * 
 * Test class of Movement class of the following robot.
 * 
 * @author Groupe suiveur
 * @version 1.4
 *
 */
public class TestMove {
	/**
	 * Test of constructor
	 * We test the creation of a Movement Class
	 */
	@Test
	public void moveTest() throws Exception {
		Movement move = new Movement('F', 14);
		assertEquals(move.getAction(), 'F');
		assertEquals(move.getDistance(), 14);
	}

}